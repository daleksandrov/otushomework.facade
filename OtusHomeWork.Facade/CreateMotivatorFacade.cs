﻿namespace OtusHomeWork.Facade
{
    /// <summary>
    /// Фасад для работы с картинками, настройкой размеров, создание и объединение слоев
    /// используется библиотека ImageMagick
    /// </summary>
    public class CreateMotivatorFacade
    {
        private readonly LayersHelper _layersHelper;
        public CreateMotivatorFacade()
        {
            MotivatorSettings settings = new MotivatorSettings();
            _layersHelper = new LayersHelper(settings);
        }

        /// <summary>
        /// Создание картинки мотиватора
        /// </summary>
        /// <param name="pathUserImage"></param>
        /// <param name="textToWrite"></param>
        public void CreateMotivator(string pathUserImage, string textToWrite)
        {
            using var textLayer = _layersHelper.GetTextLayer(textToWrite);
            
            using var userImageLayer = _layersHelper.GetUserImageLayer(pathUserImage);

            using var motivator = _layersHelper.CompositeLayers(userImageLayer, textLayer);
            
            motivator.Write(pathUserImage);
        }
    }
}