﻿using ImageMagick;

namespace OtusHomeWork.Facade
{
    /// <summary>
    /// Класс для работы со слоями
    /// </summary>
    public class LayersHelper
    {
        private readonly MotivatorSettings _settings;

        public LayersHelper(MotivatorSettings settings)
        {
            _settings = settings;
        }

        /// <summary>
        /// Добавление пользовательской картинки, текста на общий слой (фон)
        /// </summary>
        /// <param name="userImageLayer"></param>
        /// <param name="textLayer"></param>
        /// <returns></returns>
        public MagickImage CompositeLayers(MagickImage userImageLayer, MagickImage textLayer)
        {
            var motivator = new MagickImage(MagickColors.Black, _settings.SizeSettings.WidthBackLayer, _settings.SizeSettings.HeightBackLayer);

            motivator.Composite(userImageLayer, _settings.SizeSettings.LeftPadding, _settings.SizeSettings.TopPadding, CompositeOperator.Over);

            motivator.Composite(textLayer, _settings.SizeSettings.LeftPadding, _settings.SizeSettings.HeightImageLayer + 30, CompositeOperator.Over);

            return motivator;
        }

        /// <summary>
        /// Получение слоя с текстом
        /// </summary>
        /// <param name="textToWrite"></param>
        /// <returns></returns>
        public MagickImage GetTextLayer(string textToWrite)
        {
            //кажется это костыль в библиотеке ImageMagick,
            //чтобы добавить текст на картинку, нужно в параметр fileName передать ключ caption и сам текст
            var textLayer = new MagickImage($"caption:{textToWrite}", _settings.GetReadSettings());
            return textLayer;
        }

        /// <summary>
        /// Картинку пользователя добавляем на белый слой, потому что бывают картинки с прозраным фоном 
        /// </summary>
        /// <param name="pathUserImage"></param>
        /// <returns></returns>
        public MagickImage GetUserImageLayer(string pathUserImage)
        {
            //создаем белый слой
            var userImageLayer = new MagickImage(MagickColors.White, _settings.SizeSettings.WidthImageLayer, _settings.SizeSettings.HeightImageLayer);

            //пользовательскую картинку насильно подгоняем под размер белого слоя (синтаксис библиотеки imageMagick: 220x220!)
            using var userImage = new MagickImage(pathUserImage);
            userImage.Resize(new MagickGeometry($"{userImageLayer.Width}x{userImageLayer.Height}!"));

            //объединяеем белый слой и нащу картинку
            userImageLayer.Composite(userImage, Gravity.Center, CompositeOperator.Over);
            
            return userImageLayer;
        }
    }
}