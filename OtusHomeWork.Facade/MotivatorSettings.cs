﻿using ImageMagick;

namespace OtusHomeWork.Facade
{
    /// <summary>
    /// Класс для инициализации и образения к настройкам размеров и шрифтов
    /// </summary>
    public class MotivatorSettings
    {
        private SizeSettings _sizeSettings;

        public SizeSettings SizeSettings => _sizeSettings;

        public MotivatorSettings()
        {
            _sizeSettings = new SizeSettings(400, 450, 40);
        }

        /// <summary>
        /// Настройки шрифта текста
        /// </summary>
        /// <returns></returns>
        public MagickReadSettings GetReadSettings()
        {
            return new MagickReadSettings()
            {
                Font = "Comic Sans",
                FontStyle = FontStyleType.Bold,
                FontPointsize = 14,
                StrokeColor = MagickColors.White,
                TextGravity = Gravity.Center,
                BackgroundColor = MagickColors.Black,
                Height = _sizeSettings.HeightTextLayer,
                Width = _sizeSettings.WidthTextLayer
            };
        }
    }
}