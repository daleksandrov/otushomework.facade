﻿using System;
using ImageMagick;

namespace OtusHomeWork.Facade
{
    class Program
    {

        static void Main(string[] args)
        {
            try
            {
                CreateMotivatorFacade motivatorFacade = new CreateMotivatorFacade();
                
                motivatorFacade.CreateMotivator("Images/ulitka.png",
                    @"Быстрый не тот, кто быстрее бежит, а тот, кто не останавливается");
                
                motivatorFacade.CreateMotivator("Images/cat.png",
                    @"Возможность, которая появляется в вашей жизни, не будет ждать долго");
            }
            catch(Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

    }
}