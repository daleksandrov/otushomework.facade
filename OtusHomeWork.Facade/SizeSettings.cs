﻿namespace OtusHomeWork.Facade
{
    /// <summary>
    /// Класс для хранения и автоматического вычисления размеров
    /// </summary>
    public class SizeSettings
    {
        private int _widthBackLayer;
        private int _heightBackLayer;
        private int _widthImageLayer;
        private int _heightImageLayer;
        private int _leftPadding;
        private int _rightPadding;
        private int _topPadding;
        private int _bottomPadding;
        private int _widthTextLayer;
        private int _heightTextLayer;

        public SizeSettings(int widthBackLayer, int heightBackLayer, int padding)
        {
            _widthBackLayer = widthBackLayer;
            _heightBackLayer = heightBackLayer;
            _leftPadding = padding;
            _rightPadding = padding;
            _topPadding = padding;
            _bottomPadding = padding + 40;
            _widthImageLayer = widthBackLayer - 2 * padding;
            _heightImageLayer = heightBackLayer - padding - _bottomPadding;
            _widthTextLayer = _widthImageLayer;
            _heightTextLayer = _bottomPadding - 20;
        }

        public int WidthBackLayer => _widthBackLayer;

        public int HeightBackLayer => _heightBackLayer;

        public int LeftPadding => _leftPadding;

        public int TopPadding => _topPadding;
        
        public int BottomPadding => _bottomPadding;
        
        public int WidthImageLayer => _widthImageLayer;

        public int HeightImageLayer => _heightImageLayer;

        public int RightPadding => _rightPadding;

        public int WidthTextLayer => _widthTextLayer;

        public int HeightTextLayer => _heightTextLayer;
    }
}